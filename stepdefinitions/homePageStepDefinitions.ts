import { browser } from 'protractor';
import { $ } from 'protractor';
import { element } from 'protractor';
import { by } from 'protractor';
import { homePageObject } from '../pages/homePage';
import { defineSupportCode } from 'cucumber';
let chai = require('chai').use(require('chai-as-promised'));
let expect = chai.expect;

var expectedURL = "http://localhost:3000/dashboard";
var expectedBrowserTitle = "Angular Tour of Heroes";
var homePageHeader = "Tour of Heroes";
var topHerosText = "Top Heroes";

defineSupportCode(function ({ Given, Then }) {
    let homePage: homePageObject = new homePageObject();
    
    Given(/^I am on the Home Page$/, async () => {
        await expect(browser.getCurrentUrl()).to.eventually.equal(expectedURL);
    });

    Then(/^the page title is correct$/, async () => {
        await expect(browser.getTitle()).to.eventually.equal(expectedBrowserTitle);
    });

    Then(/^I can see the title displayed on the loaded page$/, async () => {
        await expect(homePage.homePageHeader.getText()).to.eventually.equal(homePageHeader);
    }); 

    Then(/^I can see the two expected routes$/, async () => {
        await expect(homePage.dashboardNavRoute.isPresent()).to.eventually.equal(true);

        await expect(homePage.heroesNavRoute.isPresent()).to.eventually.equal(true);
    });

    Then(/^I can see the four heroes listed$/, async () => {
        //Check that the heroes header appears correctly
        await expect(homePage.topHeroesHeader.getText()).to.eventually.equal(topHerosText);

        //Now check that the grid contains the four top hero modules
        var heroModules = element.all(by.xpath('.//*[@class="grid grid-pad"]/a'));
        await expect (heroModules.count()).to.eventually.equal(4);

        //Now check each module and check they have the correct hero names
        var firstHeroModule = element.all(by.xpath('.//*[@class="grid grid-pad"]/a')).get(0);
        await expect (firstHeroModule.getText()).to.eventually.equal("Mr. Nice");

        var secondHeroModule = element.all(by.xpath('.//*[@class="grid grid-pad"]/a')).get(1);
        await expect (secondHeroModule.getText()).to.eventually.equal("Narco");

        var thirdHeroModule = element.all(by.xpath('.//*[@class="grid grid-pad"]/a')).get(2);
        await expect (thirdHeroModule.getText()).to.eventually.equal("Bombasto");

        var fourthHeroModule = element.all(by.xpath('.//*[@class="grid grid-pad"]/a')).get(3);
        await expect (fourthHeroModule.getText()).to.eventually.equal("Celeritas");
    });
})