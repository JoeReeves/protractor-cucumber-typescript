@HomePageExists
Feature: Scenarios relating to home page of webapp

Scenario: The home page exists
Given I am on the Home Page
Then the page title is correct

Scenario: Home page displayes the correct page title
Given I am on the Home Page
Then I can see the title displayed on the loaded page

Scenario: Home page displays a nav bar with two routes
Given I am on the Home Page
Then I can see the two expected routes

Scenario: Home page displays a Top Heroes list
Given I am on the Home Page
Then I can see the four heroes listed