import { $ } from 'protractor';

export class homePageObject {
    public homePageHeader: any;
    public dashboardNavRoute: any;
    public heroesNavRoute: any;
    public topHeroesHeader: any;

    constructor() {
        this.homePageHeader = $("h1[_ngcontent-c0='']");
        this.dashboardNavRoute = $("a[href='/dashboard']")
        this.heroesNavRoute = $("a[href='/heroes']")
        this.topHeroesHeader = $("h3[_ngcontent-c1='']")

    }
}